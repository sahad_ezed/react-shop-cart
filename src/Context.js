import React, { createContext, useReducer } from 'react'
import {StoreProducts, DetailProduct} from './data'
import { ShopReducer, ADD_TO_CART, REMOVE_FROM_CART, OPEN_MODAL, CLOSE_MODAL, 
    INCREMENT_ITEM, DECREMENT_ITEM, CLEAR_CART, ADD_TOTAL, HANDLE_DETAIL } from './Context/Reducer'

const ProductContext = createContext()



const ProductProvider = (props) => {

    const Cstate = {
        products: StoreProducts,
        cart: [],
        detailedProduct: DetailProduct,
        modal: false,
        modalData: DetailProduct,
        cartSubtotal: 0,
        cartTax: 0,
        cartTotal: 0
    }

    
    const [cartState, dispatch] = useReducer(ShopReducer, Cstate)


    const open_modal = (id) => {
        dispatch({type: OPEN_MODAL, payload: id})
    }

    const close_modal = () => {
        dispatch({type: CLOSE_MODAL})
    }

    // cart
    const incrementItem = (id) => {
        dispatch({type: INCREMENT_ITEM, payload: id})
    }

    const decrementItem = (id) => {
        dispatch({type: DECREMENT_ITEM, payload: id})
    }

    const removeItem = (id) => {
        dispatch({type: REMOVE_FROM_CART, payload: id})
    }

    const clearCart = (id) => {
        dispatch({type: CLEAR_CART, payload: id})
    }

    const addTotal = () => {
        setTimeout(() => {
            dispatch({type: ADD_TOTAL})
        },700)
    }

    const add_to_cart = (id) => {
        dispatch({type: ADD_TO_CART, payload: id})
    }

    const handleDetail = (id) => {
        dispatch({type: HANDLE_DETAIL, payload: id})
    }


    return (
        <ProductContext.Provider value={{
            cartState: cartState,
            addCart: add_to_cart,
            handleDetail: handleDetail,
            openModal: open_modal,
            closeModal: close_modal,
            incrementItem: incrementItem,
            decrementItem: decrementItem,
            removeItem: removeItem,
            clearCart: clearCart,
            addTotal: addTotal

        }}>
            {props.children}
        </ProductContext.Provider>
    )
}


const ProductConsumer = ProductContext.Consumer;

export {ProductProvider, ProductConsumer};
