export const ADD_TO_CART = 'ADD_TO_CART'
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART'
export const OPEN_MODAL = 'OPEN_MODAL'
export const CLOSE_MODAL = 'CLOSE_MODAL'
export const INCREMENT_ITEM = 'INCREMENT_ITEM'
export const DECREMENT_ITEM = 'DECREMENT_ITEM'
export const CLEAR_CART = 'CLEAR_CART'
export const ADD_TOTAL = 'ADD_TOTAL'
export const HANDLE_DETAIL = 'HANDLE_DETAIL'


const add_to_cart = (id, state) => {
    let temProducts = [...state.products]
    const index = temProducts.indexOf(getItem(id, state.products))
    const product =  temProducts[index]
    product.inCart = true
    product.count = 1
    const price = product.price
    product.total = price * product.count
    let updatedState = { ...state, cart: [...state.cart, product], products: temProducts}

    return updatedState
}


const getItem = (id, products) => {
    const product = products.find(item => item.id === id)
    return product
}


const open_modal = (id, state) => {
    const product = getItem(id, state.products)
    let updatedState = { ...state, modal: true, modalData: product}
    return updatedState
}

const close_modal = (state) => {
    let updatedState = { ...state, modal: false }
    return updatedState
}

// cart
const incrementItem = (id, state) => {
    let temCart = [...state.cart]
    const index = temCart.indexOf(getItem(id, state.products))
    const product =  temCart[index]
    const product_count = product.count + 1
    product.count = product_count
    const price = product.price
    product.total = price * product.count
    temCart[index] = product
    let updatedState = { ...state, cart: temCart}
    return updatedState
}

const decrementItem = (id, state) => {
    let temCart = [...state.cart]
    const index = temCart.indexOf(getItem(id, state.products))
    const product =  temCart[index]

    if (product.count <= 1) {
        return state
    } else {
        const product_count = product.count - 1
        product.count = product_count
        const price = product.price
        product.total = price * product.count
        temCart[index] = product
        let updatedState = { ...state, cart: temCart}
        return updatedState
    }

    
}

const removeItem = (id, state) => {
    let temCart = [...state.cart]
    const index = temCart.indexOf(getItem(id, state.products))
    temCart.splice(index, 1);
    let updatedState = { ...state, cart: temCart}
    return updatedState
}

const clearCart = (state) => {
    let tempProduct = [...state.products]
    tempProduct.map(item => {
        return item.inCart = false
    })
    let updatedState = { ...state, cart: [], products: tempProduct}
    return updatedState
}

const addTotal = (state) => {
    let subTotal = 0
    state.cart.map(item => {
        return subTotal += item.total
    })
    const tempTax = subTotal * 0.1
    const tax = parseFloat(tempTax.toFixed(2))
    const total = subTotal + tax
    let updatedState = { ...state, cartSubtotal: subTotal, cartTax: tax, cartTotal: total}
    return updatedState

}


const handleDetail = (id, state) => {
    const DetailedProduct = (getItem(id, state.products))

    let updatedState = { ...state, detailedProduct: DetailedProduct }
    return updatedState
}




export const ShopReducer = (state, action) => {
    switch(action.type) {
        case ADD_TO_CART :
            return add_to_cart(action.payload, state)
        
        case REMOVE_FROM_CART:
            return removeItem(action.payload, state)

        case OPEN_MODAL:
            return open_modal(action.payload, state)
        
        case CLOSE_MODAL:
            return close_modal(state)

        case INCREMENT_ITEM:
            return incrementItem(action.payload, state)

        case DECREMENT_ITEM:
            return decrementItem(action.payload, state)

        case CLEAR_CART:
            return clearCart(state)

        case ADD_TOTAL:
            return addTotal(state)

        case HANDLE_DETAIL:
            return handleDetail(action.payload, state)

        default:
            return state
    }
}