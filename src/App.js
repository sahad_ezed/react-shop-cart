import React, { Fragment } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Switch, Route } from 'react-router-dom'
import './App.css'

import Navbar from './Components/Navbar'
import Product_list from './Components/Product_list'
import Details from './Components/Details'
import Cart from './Components/Cart/Cart'
import Default from './Components/Default.js'
import Modal from './Components/Modal'


function App() {
  return (
    <Fragment>
      <Navbar />

      <Switch>
        <Route exact path="/" component={Product_list} />
        <Route path="/details" component={Details} />
        <Route path="/cart" component={Cart} />
        <Route component={Default} />
      </Switch>

      <Modal />
      
    </Fragment>
  );
}

export default App;
