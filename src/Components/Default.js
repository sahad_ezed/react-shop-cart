import React from 'react'

function Default() {
    return (
        <div>
            <h1>Page not found</h1>
        </div>
    )
}

export default Default
