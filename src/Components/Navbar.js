import React from 'react'
import {Link} from 'react-router-dom'
import {ButtonContainer} from './Button'
import styled from 'styled-components'
import Logo from '../Assets/img/logo.png'

const NavWarper = styled.nav`
    background: #2a2a72;
    

    .nav-link {
        color: var(--mainWhite); !important;
        font-size: 1.3rem;
        text-transform: Capitalize;
    }
`;


function Navbar() {
    return (
        <NavWarper className="navbar navbar-fixed-top navbar-expand-sm navbar-dark px-sm-5">
            <Link to='/'>
               <img src={Logo} alt="store" style={{ width: 70 }} className="navbar-brand" />
            </Link>

            <ul className="navbar-nav align-items-center">
                <li className="nav-item ml-5">
                    <Link to="/" className="nav-link">
                        Products
                    </Link>
                </li>
            </ul>

            <Link to="/cart" className="ml-auto">
                <ButtonContainer>
                    <span className="mr-2">
                        <i className="fas fa-cart-plus" />
                    </span>
                    my cart
                </ButtonContainer>
            </Link>
        </NavWarper>
    )
}

export default Navbar
