import React from 'react'
import CartItem from './CartItem'

function CartList(props) {

    const { cart } = props.values.cartState
    return (
        <div className="container-fluid">
            {cart.map(item => {
                return (
                    <CartItem key={item.id} values={props.values} item={item} /> 
                )
            })}
            
        </div>
    )
}

export default CartList
