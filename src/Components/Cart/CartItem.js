import React from 'react'

function CartItem({item, values}) {

    const { id, title, img, price, total, count} = item
    const { incrementItem, decrementItem, removeItem } = values
    const img_path = require('../../Assets/' + img)
    
    return (
        <div className="row my-1 mb-3 mt-2 text-capitalize text-center">
            <div className="col-10 mx-auto col-lg-2">
                <img src={img_path} style={{width: '5rem', height: '5rem' }} className="img-fluid" alt="product" />
            </div>

            <div className="col-10 mx-auto col-lg-2">
                <span className="d-lg-none">Product : </span> {title}
            </div>

            <div className="col-10 mx-auto col-lg-2">
                <span className="d-lg-none">Price : </span> {price}
            </div>

            <div className="col-10 mx-auto col-lg-2 my-2 my-lg-0">
                <div className="d-flex justify-content-center">
                    <div>
                        <span className="btn btn-black mx-1" onClick={() => decrementItem(id)}>-</span>
                        <span className="btn btn-black mx-1">{count}</span>
                        <span className="btn btn-black mx-1" onClick={() => incrementItem(id)}>+</span>
                    </div>
                </div>
            </div>


            <div className="col-10 mx-auto col-lg-2" onClick={() => removeItem(id)}>
                <div className="cart-icon">
                    <i className="fas fa-trash"></i>
                </div>
            </div>

            <div className="col-10 mx-auto col-lg-2">
                <strong>item total : $ {total}</strong>
            </div>
        </div>
    )
}

export default CartItem
