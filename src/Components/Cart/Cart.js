import React, { Fragment } from 'react'
import Title from '../Title'
import CartColumn from './CartColumn'
import EmptyCart from './EmptyCart'
import CartList from './CartList'
import CartTotal from './CartTotal'

import { ProductConsumer } from '../../Context'

function Cart() {
    return (
        <section>
            <ProductConsumer>
                {value => {
                    value.addTotal()  //for add total price
                    const {cart} = value.cartState
                    if(cart.length > 0) {
                        return (
                            <Fragment>
                                <Title name="Your" title="cart"></Title>
                                <CartColumn />
                                <CartList values={value} />
                                <CartTotal values={value} />
                            </Fragment>
                        )
                    } else {
                        return (
                            <EmptyCart />
                        )
                    }
                }}
            </ProductConsumer>
        </section>
    )
}

export default Cart
