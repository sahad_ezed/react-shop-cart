import React from 'react'
import {Link} from 'react-router-dom'
import {ButtonContainer} from './Button'
import { ProductConsumer } from '../Context'

function Details() {
    return (
        <ProductConsumer>
            {value => {

                const { id, company, img, info, price, title, inCart } = value.cartState.detailedProduct
                const img_path = require('../Assets/' + img)
                
                return (
                    <div className="container py-5">
                        {/* title */}
                        <div className="row">
                            <div className="col-10 mx-auto text-center text-slanted text-blue my-5">
                            <h1> {title} </h1>
                            </div>
                        </div>

                        {/* product-info */}
                        <div className="row">
                            <div className="col-10 mx-auto col-md-6 my-3 text-capitalize">
                                <img src={img_path} className="img-fluid" alt="product" />
                            </div>

                            {/* product-info */}
                            <div className="col-10 mx-auto col-md-6 my-3 text-capitalize">
                                <h2>model: {title}</h2>
                                <h4 className="text-title text-uppercase text-muted mt-3 mb-2">
                                    made by: <span className="text-uppercase"> {company} </span>
                                </h4>
                                <h4 className="text-blue">
                                   <strong> price: <span> $ </span> {price} </strong>
                                </h4>
                                <p className="text-capitalize font-weight-bold mt-3 mb-0">
                                    Some info about product
                                </p>
                                <p className="text-muted lead">{info}</p>

                                {/* buttons */}
                                <Link to="/">
                                    <ButtonContainer>
                                        Back to Product
                                    </ButtonContainer>
                                </Link>

                                <ButtonContainer cart disabled={inCart?true:false} onClick={() => value.addCart(id)}>
                                    {inCart?"In Cart": "add to cart"}
                                </ButtonContainer>
                                
                            </div>
                        </div>

                    </div>
                )
            }}
        </ProductConsumer>
    )
}

export default Details
