import React, { Fragment } from 'react'
import { ProductConsumer } from '../Context'
import Product from './Product'
import Title from './Title'

function Product_list() {
    
    return (
        <Fragment>
            <div className="py-5">
                <div className="container">
                    <Title name="Our" title="Product" />
                    <div className="row">
                        <ProductConsumer>
                            {value => {
                                console.log('tessssdfsdfsdfsdfsdf')
                                console.log(value)
                                return (
                                    value.cartState.products.map(product => {
                                        return <Product key={product.id} product={product} />
                                    })
                                )
                            }}
                        </ProductConsumer>
                        
                    </div>
                </div>

            </div>
        </Fragment>
    )
}

export default Product_list
